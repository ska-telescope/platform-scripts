# platform-scripts

This repository contains the scripts that are being developed within PLANET team for the benchmarking of SKA SDP pipelines.
Currently available scripts are:

- _ska-sdp-benchmark-suite_ This is a prototype benchmark suite to deploy synthetic SDP benchmarks on production machines. Currently, the only benchmark available is [Distributed Imaging I/O prototype](https://gitlab.com/ska-telescope/sdp/ska-sdp-exec-iotest).
- _ska-sdp-monitor-cpu-metrics_ This is another prototype to monitor SDP pipelines/workflows that are submitted with SLURM job scheduler. The development of this prototype is migrated to a more mature toolkit that is hosted at a new [repository](https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon). Please use new repository for monitoring performance metrics.

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-platform-scripts/badge/?version=latest)](https://developer.skao.int/projects/platform-scripts/en/latest/?badge=latest)

More details of each package can be found in the [Documentation](https://developer.skao.int/projects/platform-scripts/en/latest/?badge=latest).
