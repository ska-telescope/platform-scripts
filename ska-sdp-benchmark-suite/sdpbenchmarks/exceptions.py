"""This module contains the custom exceptions defined for benchmark suite."""


class ExecuteCommandError(Exception):
    """Command execution exception"""
    pass


class KeyNotFoundError(Exception):
    """Key missing in the dict"""
    pass


class JobSubmissionError(Exception):
    """Job submission to batch scheduler failed"""
    pass


class JobScriptCreationError(Exception):
    """Error in generating the job script to submit"""
    pass


class ExportError(Exception):
    """Error in exporting results"""
    pass


class BenchmarkError(Exception):
    """Error in benchmarking the codes"""
    pass


class ImagingIOTestError(Exception):
    """Error in Imaging IO test benchmakr run"""
    pass
