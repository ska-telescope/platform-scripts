#!/usr/bin/env python3
""" Main script to run the SDP benchmarks. """

import time
import argparse
import logging
import sys
import socket
import platform
import os
import yaml

# Add current directory to PYTHONPATH
try:
    parent_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    sys.path.index(parent_dir)
except ValueError:
    sys.path.insert(0, parent_dir)

try:
    import importlib.resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources

from sdpbenchmarks import config
from sdpbenchmarks.config import benchmarks
from sdpbenchmarks.sdpbenchmarkengine import SdpBenchmarkEngine

from sdpbenchmarks._version import __version__


class bColors:
    """ Colors for terminal output """
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def main():
    """ sdpbenchrun. """
    parser = argparse.ArgumentParser(
        description="SKA SDP Pipeline Benchmark Tests",
        epilog="----------------------------------------------------",
    )

    # Arguments
    parser.add_argument("-t", "--tag",
                        nargs='?',
                        help="Run tag",
                        default=None)

    parser.add_argument("-b", "--benchmarks",
                        nargs='+',
                        help="List of benchmarks to run.",
                        default=None)

    parser.add_argument("-c", "--config",
                        nargs='?',
                        type=str,
                        help=
                        "Configuration file to use. If not provided, load default config file.",
                        default=None)

    parser.add_argument("-m", "--run_mode",
                        # choices=['singularity', 'bare-metal'],
                        nargs='+',
                        help="Run benchmarks in container and/or bare metal.",
                        default=None)

    parser.add_argument("-n", "--num_nodes",
                        nargs='+',
                        type=int,
                        help="Number of nodes to run the benchmarks.",
                        default=None)

    parser.add_argument("-d", "--work_dir",
                        nargs='?',
                        help="Directory where benchmarks will be run.",
                        default=None)

    parser.add_argument("-s", "--scratch_dir",
                        nargs='?',
                        help="Directory where benchmark output files will be stored.",
                        default=None)

    parser.add_argument("-j", "--submit_job",
                        action='store_true',
                        help="Run the benchmark suite in job submission mode.",
                        default=None)

    parser.add_argument("-p", "--repetitions",
                        nargs='?',
                        type=int,
                        help="Number of repetitions of benchmark runs.",
                        default=None)

    parser.add_argument("-o", "--randomise",
                        action='store_true',
                        help="Randomise the order of runs.",
                        default=None)

    parser.add_argument("--ignore_rem",
                        action='store_true',
                        help="In case of unfinished jobs, skip remaining jobs on relaunch.",
                        default=None)

    parser.add_argument("-e", "--export",
                        action='store_true',
                        help="Export all stdout, json and log files from run_dir and compresses "
                             "them.",
                        default=None)

    parser.add_argument("-a", "--show",
                        action='store_true',
                        help="Show running config and exit.",
                        default=None)

    parser.add_argument("-l", "--list_configs",
                        action='store_true',
                        help="List all the pre-defined config files.",
                        default=None)

    parser.add_argument("-v", "--verbose",
                        action='store_true',
                        help="Enable verbose mode. Display debug messages.",
                        default=None)

    parser.add_argument('--version',
                        action='version',
                        version='{version}'.format(version=__version__))

    args = parser.parse_args()

    # Predefined configs
    PREDEFINED_CONFIGS = ["default", "compute_bm_ska1_low_small", "io_bm_ska1_low_small"]
    # Print the help message of each config file and exit
    if args.list_configs:
        for cfg_name in PREDEFINED_CONFIGS[1:]:
            cfg_file = cfg_name + ".yml"
            try:
                with pkg_resources.path(benchmarks, cfg_file) as cfg:
                    with open(cfg, 'r') as yam:
                        help_msg = yaml.full_load(yam)['help']
                    print("\n{}{}{}".format(bColors.OKBLUE, cfg_name, bColors.ENDC))
                    print("\t{}".format(help_msg))
            except Exception:
                pass
        sys.exit(0)

    # Check if name of the benchmark run is provided
    if not args.tag:
        print("{}No name given for the benchmark run.{}".format(bColors.OKCYAN, bColors.ENDC))
        print("{}Using timestamp as the name of the benchmark run {}".format(bColors.OKBLUE,
                                                                             bColors.ENDC))
        args.tag = "_".join([socket.gethostname(), str(int(time.time()))])

    # Select the config file to load
    # load default configuration shipped with package if
    # No configuration file was provided
    if args.config in PREDEFINED_CONFIGS or args.config is None:
        if args.config is None or args.config == "default":
            print("{}Running with default configuration.{}".format(bColors.OKCYAN, bColors.ENDC))
            args.config = "default_config.yml"
            try:
                with pkg_resources.path(config, args.config) as default_cfg:
                    load_config = default_cfg
            except Exception:
                print(
                    "{}Unable to load {} yaml configuration.{}".format(bColors.FAIL, args.config,
                                                                       bColors.ENDC))
                sys.exit(1)
        else:
            args.config += ".yml"
            try:
                with pkg_resources.path(benchmarks, args.config) as cfg:
                    load_config = cfg
            except Exception:
                print(
                    "{}Unable to load {} yaml configuration.{}".format(bColors.FAIL, args.config,
                                                                       bColors.ENDC))
                sys.exit(1)

    else:
        load_config = args.config

    # Load configuration file
    try:
        with open(load_config, 'r') as yam:
            active_config = yaml.full_load(yam)
    except FileNotFoundError:
        print("{0}Failed to load configuration file: {1} {2}".format(bColors.FAIL, load_config,
                                                                     bColors.ENDC))
        sys.exit(1)

    # Check for cli overrides
    # Convert arguments to dict
    temp_config = vars(args)
    del temp_config['config']

    # Get non-None cli arguments to override config file
    non_empty = {k: v for k, v in temp_config.items() if v is not None}

    # Populate active config with cli override
    for i in non_empty.keys():
        active_config['global'][i] = non_empty[i]

    # Check if user provided a benchmark
    if active_config['global']['benchmarks'] is None:
        parser.print_help()
        print("{}No benchmarks were selected. {}".format(bColors.FAIL, bColors.ENDC))
        sys.exit(1)

    # Check if user provided valid benchmark
    AVAILABLE_BENCHMARKS = ("iotest",)
    for bench in active_config['global']['benchmarks']:
        if bench not in AVAILABLE_BENCHMARKS:
            print('{}Benchmark "{}" is not a valid benchmark.{}'.format(bColors.FAIL, bench,
                                                                        bColors.ENDC))
            print('Please select one of the following benchmarks:\n- {}'.format(
                '\n- '.join(AVAILABLE_BENCHMARKS)))
            sys.exit(1)
        if bench not in active_config.keys():
            print('{}Benchmark "{}" config is not found the config file.{}'.format(bColors.FAIL,
                                                                                   bench,
                                                                                   bColors.ENDC))
            print('Please check the provided config file.\n')
            sys.exit(1)

    # Load system specific config file. By default the package copies the config file to home
    # directory of the user. If user modifies it, the user defined config file takes precedence.
    COMPILER_CONFIG_FILE = ".ska_sdp_bms_system_config.yml"
    with pkg_resources.path(config, COMPILER_CONFIG_FILE) as mpi_cfg:
        with open(mpi_cfg, 'r') as yam:
            active_config.update(yaml.full_load(yam))

    # Read the config file in the home directory if exists and update the configuration
    try:
        with open(os.path.join(os.path.expanduser('~'), COMPILER_CONFIG_FILE), 'r') as yam_user:
            active_config.update(yaml.full_load(yam_user))
    except FileNotFoundError:
        print("{}No system config file found in the home directory of the user. Using default{}".
              format(bColors.OKCYAN, bColors.ENDC))
        pass

    # Check if node count in config is integer
    if not all(isinstance(n, int) for n in active_config['global']['num_nodes']):
        print("{}Non integer number of nodes found. Exiting...{}".format(bColors.FAIL,
                                                                         bColors.ENDC))
        sys.exit(1)

    # Check if repetitions count in config is integer
    if not isinstance(active_config['global']['repetitions'], int):
        print("{}Non integer number of repetitions found. Exiting...{}".format(bColors.FAIL,
                                                                               bColors.ENDC))
        sys.exit(1)

    # Check if interactive mode is using on MacOS
    if platform.system() == "Darwin" and active_config['global']['submit_job']:
        print("{}You are running on MacOS. Please choose interactive mode. Exiting....{}".
              format(bColors.FAIL, bColors.ENDC))
        sys.exit(1)

    print("# The following configuration was loaded: {}".format(load_config))
    if len(non_empty):
        print("# The configuration was overridden by the following CLI args: {}".format(non_empty))

    # Print running configuration and exit
    if args.show:
        print(yaml.dump(active_config))
        sys.exit(0)

    # Create another dict key to mark work_dir and scratch_dir
    # Append the date to the work_dir in order to group the results per date
    active_config['global']['work_dir'] = os.path.abspath(os.path.expanduser(os.path.expandvars(
        active_config['global']['work_dir'])))
    active_config['global']['scratch_dir'] = \
        os.path.abspath(os.path.expanduser(os.path.expandvars(active_config['global'][
                                                                  'scratch_dir'])))

    # Create run_dir, result_dir and script_dir
    for bench_name in active_config['global']['benchmarks']:
        # Working directory of the benchmark
        active_config[bench_name]['work_dir'] = os.path.join(active_config['global']['work_dir'],
                                                             bench_name)
        os.makedirs(active_config[bench_name]['work_dir'], exist_ok=True)

        # Directory where all output files are placed
        active_config[bench_name]['out_dir'] = os.path.join(active_config[bench_name][
                                                                'work_dir'], 'out')
        os.makedirs(active_config[bench_name]['out_dir'], exist_ok=True)

        # Directory where all output files are placed
        active_config[bench_name]['result_dir'] = os.path.join(active_config[bench_name][
                                                                   'work_dir'], 'out', 'std-out')
        os.makedirs(active_config[bench_name]['result_dir'], exist_ok=True)

        # Directory where json outputs are placed
        active_config[bench_name]['json_dir'] = os.path.join(active_config[bench_name][
                                                                 'work_dir'], 'out', 'json-out')
        os.makedirs(active_config[bench_name]['json_dir'], exist_ok=True)

        # Directory where job reservation scripts are placed
        active_config[bench_name]['script_dir'] = os.path.join(active_config[bench_name][
                                                                   'work_dir'], 'scripts')
        os.makedirs(active_config[bench_name]['script_dir'], exist_ok=True)

    # Create persistence directory for tracking experiment runs
    active_config['global']['per_dir'] = os.path.join(active_config['global']['work_dir'],
                                                      'experiment_persist',
                                                      active_config['global']['tag'])

    if os.path.isdir(active_config['global']['per_dir']):
        print("{}Persistence directory exists. Resuming run {}.{}".format(bColors.OKCYAN,
                                                                          active_config[
                                                                              'global'][
                                                                              'tag'],
                                                                          bColors.ENDC))
        if active_config['global']['ignore_rem']:
            print("{}Ignoring remaining runs.{}".format(bColors.WARNING, bColors.ENDC))
    else:
        # If there no persistence directory, never skip any runs
        active_config['global']['ignore_rem'] = False

        # Configure logging
    # Log verbosity
    if args.verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO

    # Enable logging
    logger = logging.getLogger()
    logger.setLevel(log_level)

    # Log format
    log_formatter = logging.Formatter(
        '%(version)s | %(asctime)s | %(levelname)s | %(threadName)s | %(name)s:%(funcName)s '
        '| %(lineno)d || %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')

    # Handler to write logs to stdout
    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setFormatter(log_formatter)
    stream_handler.setLevel(log_level)

    # Handler to write logs to file
    LOG_PATH = os.path.join(active_config['global']['work_dir'], 'ska_sdp_benchmarks.log')
    file_handler = logging.FileHandler(LOG_PATH)
    file_handler.setFormatter(log_formatter)
    file_handler.setLevel(log_level)

    # Select loggers
    logger.addHandler(stream_handler)
    logger.addHandler(file_handler)

    # Add version to the logger according to SKA standards
    logger = logging.LoggerAdapter(logger, {'version': __version__})

    # Save running config
    with open(os.path.join(active_config['global']['work_dir'], 'run_config.yml'),
              'w') as conf_file:
        yaml.dump(active_config, conf_file)

    # Configure ska_sdp_benchmarks
    logger.debug("Active configuration in use: %s", active_config)
    test = SdpBenchmarkEngine(config=active_config)

    try:
        test.start()
    except Exception:
        logger.warning(
            "SDP Benchmark Engine has failed benchmarks. Please be aware of the results.")

    print("{}Full run log can can be found in {} {}".format(bColors.OKCYAN, LOG_PATH, bColors.ENDC))


if __name__ == "__main__":
    main()
