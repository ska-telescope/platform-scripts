# SKA SDP Benchmark Suite

This package contains the SDP benchmark suite to deploy different benchmarks using one application. Currently, only distributed Imaging I/O prototype is included in the suite.

More information on how to install, use the benchmark suite can be found in the [documentation](https://developer.skao.int/projects/platform-scripts/en/latest/?badge=latest).
