#!/usr/bin/env python3
""" This script does the unit tests for the SDP benchmarks utils"""

import os
import sys
import subprocess
import platform
import shutil
import unittest
from unittest.mock import patch
import yaml

try:
    parent_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    sys.path.index(parent_dir)
except ValueError:
    sys.path.insert(0, parent_dir)

from sdpbenchmarks import utils
from sdpbenchmarks.exceptions import ExecuteCommandError, JobSubmissionError, KeyNotFoundError


class TestUtils(unittest.TestCase):
    """Class that tests utils functions"""

    def setup(self):
        """Load CI configuration"""

        project_root = utils.get_project_root()

        with open(os.path.join(project_root, "tests/ci/ci-config.yml"), 'r') as cfg_file, \
                open(os.path.join(project_root, "tests/ci/ci-system-config.yml"),
                     'r') as sys_file:
            self.config_file = yaml.full_load(cfg_file)
            self.config_file.update(yaml.full_load(sys_file))
            self.config_file['iotest']['work_dir'] = '.'

    def test_pull_image(self):
        """Test if pulling image from registry"""

        for uri in ["docker://mock1", "oras://mock2"]:
            with self.subTest():
                self.assertEqual(utils.pull_image(uri, "singularity", "test"), 1)

    def test_sweep(self):
        """Test parameter sweep"""

        param = {
            'arg_1': [1, 2],
            'arg_2': ['foo', 'bar']
        }

        result = [{'arg_1': 1, 'arg_2': 'foo', 'run_prefix': '1_foo'},
                  {'arg_1': 1, 'arg_2': 'bar', 'run_prefix': '1_bar'},
                  {'arg_1': 2, 'arg_2': 'foo', 'run_prefix': '2_foo'},
                  {'arg_1': 2, 'arg_2': 'bar', 'run_prefix': '2_bar'}]

        assert utils.sweep(param) == result

    @patch.object(platform, 'system', return_value="Linux")
    @patch.object(subprocess, 'run')
    def test_get_sockets_cores_linux(self, mock_run, mock_system):
        """Test the socket core function for linux kernel"""

        self.setup()
        test_config = self.config_file.copy()
        # test_config['global']['submit_job'] = False

        sample_output = lambda: None
        sample_output.stdout = r"2"
        mock_run.return_value = sample_output

        assert utils.get_sockets_cores(test_config) == (2, 4, 2)

    @patch.object(platform, 'system', return_value="Darwin")
    @patch.object(subprocess, 'run')
    def test_get_sockets_cores_linux(self, mock_run, mock_system):
        """Test the socket core function for Darwin kernel"""

        self.setup()
        test_config = self.config_file.copy()
        # test_config['global']['submit_job'] = False

        sample_output = lambda: None
        sample_output.stdout = r"2"
        mock_run.return_value = sample_output

        assert utils.get_sockets_cores(test_config) == (2, 4, 1)

    @patch.object(shutil, 'which', return_value="SLURM")
    @patch.object(subprocess, 'run')
    def test_get_sockets_cores_sinfo(self, mock_run, mock_system):
        """Test the socket core function for Darwin kernel"""

        self.setup()
        test_config = self.config_file.copy()
        test_config['global']['submit_job'] = True

        sample_output = lambda: None
        sample_output.stdout = "2:16:2"
        mock_run.return_value = sample_output

        assert utils.get_sockets_cores(test_config) == (2, 32, 2)

    @patch.object(subprocess, 'run')
    def test_job_status(self, mock_run):
        """Test batch job status"""

        self.setup()
        test_config = self.config_file.copy()
        test_config['scheduler']['name'] = "OAR"

        sample_output = lambda: None
        param_dict = {
            'INPROGRESS': ["Waiting", "Running", "Finishing"],
            'SUCCESS':  ["Terminated"],
            'FAIL': ["Error"]
        }
        for res, cmd_out in param_dict.items():
            for out in cmd_out:
                sample_output.stdout = out.encode('utf-8')
                mock_run.return_value = sample_output
                with self.subTest():
                    self.assertEqual(utils.get_job_status(test_config['scheduler'], 12345), res)

        test_config['scheduler']['name'] = "SLURM"

        param_dict = {
            'INPROGRESS': ["PENDING", "PD", "RUNNING", "R", "PREEMPTED", "PR", "CONFIGURING", "CF",
                       "COMPLETING", "CG", "RESV_DEL_HOLD", "RD"],
            'SUCCESS': ["COMPLETED", "CD"],
            'FAIL': ["FAILED", "F", "NODE_FAIL", "NF", "TIMEOUT", "TO", "CANCELLED", "CA",
                     "DEADLINE", "DL", "OUT_OF_MEMORY", "OOM", "SUSPENDED", "S",
                     "BOOT_FAIL", "BF", "STOPPED", "ST"]
        }
        for res, cmd_out in param_dict.items():
            for out in cmd_out:
                sample_output.stdout = out.encode('utf-8')
                mock_run.return_value = sample_output
                with self.subTest():
                    self.assertEqual(utils.get_job_status(test_config['scheduler'], 12345), res)

    @patch.object(subprocess, 'run')
    @patch.object(utils, 'write_oar_job_file', return_value="temp")
    def test_submit_oar_job_fail(self, mock_write_oar_job_file, mock_run):
        """Tests the failed submission of OAR job"""

        self.setup()
        test_config = self.config_file.copy()
        test_config['scheduler']['name'] = "OAR"
        test_config['scheduler']['cmd_str'] = "cmd_str"
        test_config['scheduler']['run_prefix'] = "run_prefix"

        sample_output = lambda: None
        sample_output.stdout = "OAR_JOB_ID=-1\n"
        mock_run.return_value = sample_output
        with self.subTest():
            with self.assertRaises(JobSubmissionError):
                utils.submit_job(test_config['scheduler'], 123)

    @patch.object(subprocess, 'run')
    @patch.object(utils, 'write_oar_job_file', return_value="temp")
    def test_submit_oar_job_pass(self, mock_write_oar_job_file, mock_run):
        """Tests the passed submission of OAR job"""

        self.setup()
        test_config = self.config_file.copy()
        test_config['scheduler']['name'] = "OAR"
        test_config['scheduler']['cmd_str'] = "cmd_str"
        test_config['scheduler']['run_prefix'] = "run_prefix"

        sample_output = lambda: None
        sample_output.stdout = "OAR_JOB_ID=12345\n"
        mock_run.return_value = sample_output
        with self.subTest():
            self.assertEqual(utils.submit_job(test_config['scheduler'], 123), 12345)

    @patch.object(subprocess, 'run')
    @patch.object(utils, 'write_slurm_job_file', return_value="temp")
    def test_submit_slurm_job_fail(self, mock_write_slurm_job_file, mock_run):
        """Tests the failed submission of SLURM job"""

        self.setup()
        test_config = self.config_file.copy()
        test_config['scheduler']['name'] = "SLURM"
        test_config['scheduler']['cmd_str'] = "cmd_str"
        test_config['scheduler']['run_prefix'] = "run_prefix"

        sample_output = lambda: None
        sample_output.stdout = "sbatch: error: Batch job submission failed\n"
        mock_run.return_value = sample_output
        with self.subTest():
            with self.assertRaises(JobSubmissionError):
                utils.submit_job(test_config['scheduler'], 123)

    @patch.object(subprocess, 'run')
    @patch.object(utils, 'write_slurm_job_file', return_value="temp")
    def test_submit_slurm_job_pass(self, mock_write_slurm_job_file, mock_run):
        """Tests the passed submission of SLURM job"""

        self.setup()
        test_config = self.config_file.copy()
        test_config['scheduler']['name'] = "SLURM"
        test_config['scheduler']['cmd_str'] = "cmd_str"
        test_config['scheduler']['run_prefix'] = "run_prefix"

        sample_output = lambda: None
        sample_output.stdout = "Submitted batch job 12345\n"
        mock_run.return_value = sample_output
        with self.subTest():
            self.assertEqual(utils.submit_job(test_config['scheduler'], 123), 12345)

    def test_create_scheduler_conf(self):
        """Test creation of scheduler config"""

        self.setup()
        test_config = self.config_file.copy()
        test_config['scheduler']['name'] = "SLURM"

        with self.assertRaises(KeyNotFoundError):
            utils.create_scheduler_conf(test_config, {}, "test")

    def test_standardise_output_data(self):
        """Test standardising output"""

        self.setup()
        test_config = self.config_file.copy()
        test_config['scheduler']['name'] = "SLURM"

        with self.assertRaises(KeyNotFoundError):
            utils.standardise_output_data("test", test_config, {}, {})

    @patch.object(subprocess, 'run')
    def test_execute_command_on_host(self, mock_run):
        """Test standardising output"""

        mock_run.side_effect = subprocess.CalledProcessError(returncode=2, cmd=["bad command"])

        with self.assertRaises(ExecuteCommandError):
            utils.execute_command_on_host("test", "temp")
        os.remove("temp")


if __name__ == '__main__':
    unittest.main(verbosity=2)
