#!/usr/bin/env python3
""" This script does the unit tests for the SDP benchmarks """

import os
import sys
import unittest
from unittest.mock import patch
import shutil
import yaml

try:
    parent_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    sys.path.index(parent_dir)
except ValueError:
    sys.path.insert(0, parent_dir)

from sdpbenchmarks.sdpbenchmarkengine import SdpBenchmarkEngine
from sdpbenchmarks import imagingiobench, utils
from sdpbenchmarks.exceptions import ExportError, BenchmarkError


class TestSuite(unittest.TestCase):
    """Test the SDP benchmark driver"""

    def setup(self):
        """Load CI configuration"""

        project_root = utils.get_project_root()

        with open(os.path.join(project_root, "tests/ci/ci-config.yml"), 'r') as cfg_file, \
                open(os.path.join(project_root, "tests/ci/ci-system-config.yml"),
                     'r') as sys_file:
            self.config_file = yaml.full_load(cfg_file)
            self.config_file.update(yaml.full_load(sys_file))
            self.config_file['iotest']['work_dir'] = '.'

    def test_preflight(self):
        """Test the preflight failures"""

        self.setup()
        test_config = self.config_file.copy()

        test_config['global']['run_mode'] = ["bare-metal", "singularity", "docker"]
        test_config['iotest']['image'] = "oras://mock-uri"

        shutil.which = lambda x: None

        test = SdpBenchmarkEngine(config=test_config)

        with self.assertLogs('sdpbenchmarks.sdpbenchmarkengine', level='INFO') as log:
            with self.assertRaises(Exception):
                test.start()
            self.assertIn("ERROR:sdpbenchmarks.sdpbenchmarkengine:Invalid run mode specified: "
                          "['bare-metal', 'singularity', 'docker'].", " ".join(log.output))
            self.assertIn("ERROR:sdpbenchmarks.sdpbenchmarkengine:"
                          "Cannot pull the singularity image file.", " ".join(log.output))

        self.clean_up(test_config)

    @patch.object(SdpBenchmarkEngine, 'pre_flight', return_code=1)
    def test_run(self, mock_pre_flight):
        """Test the run function"""

        self.setup()
        test_config = self.config_file.copy()

        imagingiobench.prepare_iotest = lambda conf: 0
        imagingiobench.check_iotest_arguments = lambda conf: 0
        imagingiobench.run_iotest = lambda conf: 0

        test = SdpBenchmarkEngine(config=test_config)

        with self.assertLogs('sdpbenchmarks.sdpbenchmarkengine', level='INFO') as log:
            test.start()
            self.assertIn("INFO:sdpbenchmarks.sdpbenchmarkengine:"
                          "Terminated Imaging IO bench with success", " ".join(log.output))

    def test_cleanup(self):
        """Test if cleanup raises exceptions"""

        self.setup()
        test_config = self.config_file.copy()

        test = SdpBenchmarkEngine(config=test_config)

        test.failures = [1]
        with self.subTest():
            with self.assertRaises(BenchmarkError):
                test.cleanup()

        test_config['global']['export'] = True

        with self.subTest():
            with self.assertRaises(ExportError):
                test.cleanup()

    def clean_up(self, config):
        """Deletes directories test created"""

        shutil.rmtree(config['global']['work_dir'])
        shutil.rmtree(config['global']['scratch_dir'])


if __name__ == '__main__':
    unittest.main(verbosity=2)
