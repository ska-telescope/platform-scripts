#!/usr/bin/env python3
""" This script does the unit tests for the SDP benchmarks utils"""

import os
import sys
import subprocess
import unittest
from unittest.mock import patch
import yaml

try:
    parent_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    sys.path.index(parent_dir)
except ValueError:
    sys.path.insert(0, parent_dir)

from sdpbenchmarks import imagingiobench, utils
from sdpbenchmarks.exceptions import ExecuteCommandError


class TestImagingIoTest(unittest.TestCase):
    """Class that tests imagingiotest functions"""

    def setup(self):
        """Load CI configuration"""

        project_root = utils.get_project_root()

        with open(os.path.join(project_root, "tests/ci/ci-config.yml"), 'r') as cfg_file, \
                open(os.path.join(project_root, "tests/ci/ci-system-config.yml"),
                     'r') as sys_file:
            self.config_file = yaml.full_load(cfg_file)
            self.config_file.update(yaml.full_load(sys_file))
            self.config_file['iotest']['work_dir'] = '.'

        self.param_conf = {
            'tag': ["test"],
            'run_mode': 'bare-metal',
            'num_nodes': 2,
            'rec_set': ['small'],
            'vis_set': ['vlaa'],
            'time_chunk': [256],
            'freq_chunk': [256],
            'run_num': [0, 1, 2]
        }

        imagingiobench.BENCH_NAME = 'iotest'
        imagingiobench.GIT_REPO_NAME = "MOCK_REPO"
        imagingiobench.NUMBER_OF_FACETS = {
            **dict.fromkeys(['tiny', '8k-2k-512'], 2),
            **dict.fromkeys(['small', '16k-8k-512'], 9),
            **dict.fromkeys(['smallish', '32k-8k-1k'], 25),
            **dict.fromkeys(['medium', '64k-16k-1k'], 25),
            **dict.fromkeys(['large', '96k-12k-1k'], 81),
            **dict.fromkeys(['tremendous', '128k-32k-2k'], 25),
            **dict.fromkeys(['huge', '256k-32k-2k'], 81),
            '96k-24k-1k': 25,
            '128k-32k-1k': 25,
            'T05_': 1
        }

    def test_create_bench_conf(self):
        """Test the creation of benchmark config file"""

        self.setup()
        conf = self.param_conf.copy()

        assert imagingiobench.create_bench_conf(conf['tag'][0], conf['run_mode'],
                                                conf['num_nodes'],
                                                len(conf['run_num']), conf['rec_set'],
                                                conf['vis_set'],
                                                [str(conf['time_chunk'][0]) +
                                                 "," +
                                                 str(conf['freq_chunk'][0])]) == conf

    def test_check_iotest_arguments_unknown_rec_set(self):
        """Tests the iotest arguments checker for unknown --rec-set parameter"""

        self.setup()
        test_config = self.config_file.copy()

        test_config['iotest']['iotest_args']['rec_set'] = ["unknown"]

        assert imagingiobench.check_iotest_arguments(test_config) == 1

    def test_check_iotest_arguments_unknown_vis_set(self):
        """Tests the iotest arguments checker for unknown --vis-set parameter"""

        self.setup()
        test_config = self.config_file.copy()

        test_config['iotest']['iotest_args']['vis_set'] = ["unknown"]

        assert imagingiobench.check_iotest_arguments(test_config) == 1

    def test_check_iotest_arguments_insufficient_space(self):
        """Tests the iotest arguments checker for disk space on scratch"""

        self.setup()
        test_config = self.config_file.copy()

        test_config['iotest']['iotest_args']['vis_set'] = ["lowbd2"]
        test_config['iotest']['iotest_args']['write_vis'] = True
        test_config['global']['scratch_dir'] = "temp"
        test_config['global']['avail_scratch_disk_space'] = 100

        imagingiobench.GEN_VIS_DATA = {
            'lowbd2': 17000,
            'midr5': 14000
        }

        assert imagingiobench.check_iotest_arguments(test_config) == 1

    @patch.object(subprocess, 'run')
    def test_compile_imaging_iotest_pass(self, mock_run):
        """Test the passing of compiling Imaging IO test code"""

        self.setup()
        test_config = self.config_file.copy()

        sample_output = lambda: None
        sample_output.stdout = r"Installation Successful"
        sample_output.returncode = 0
        mock_run.return_value = sample_output

        assert imagingiobench.compile_imaging_iotest(test_config) == 0

    @patch.object(subprocess, 'run')
    @patch.object(utils, 'log_failed_cmd_stderr_file')
    def test_compile_imaging_iotest_fail(self, mock_log_failed_cmd_stderr_file, mock_run):
        """Test the failing of compiling Imaging IO test code"""

        self.setup()
        test_config = self.config_file.copy()

        mock_log_failed_cmd_stderr_file.return_value = "file_path"
        mock_run.side_effect = subprocess.CalledProcessError(returncode=2, cmd=["bad command"])

        with self.assertRaises(ExecuteCommandError):
            imagingiobench.compile_imaging_iotest(test_config)

    @patch.object(os.path, 'isfile')
    def test_install_iotest(self, mock_isfile):
        """Test if Imaging IO test code exists"""

        self.setup()
        test_config = self.config_file.copy()

        mock_isfile.return_value = True

        assert imagingiobench.prepare_iotest(test_config) == 0

    def test_get_mpi_args_mpirun(self):
        """Test MPI argument string for mpirun startup"""

        self.setup()
        test_config = self.config_file.copy()

        num_nodes = 2
        num_omp_threads = 16
        num_processes = 2

        mpi_cmd_str = "mpirun --allow-run-as-root --mca mpi_yield_when_idle 1 " \
                      "--mca " \
                      "mca_base_env_list " \
                      "\'I_MPI_JOB_RESPECT_PROCESS_PLACEMENT=0;I_MPI_PIN_DOMAIN=omp:compact;" \
                      "OMP_PROC_BIND=true;OMP_PLACES=sockets;OMP_NUM_THREADS=16\' " \
                      "--tag-output --timestamp-output --map-by node -np 2 "

        assert imagingiobench.get_mpi_args(test_config, num_nodes, num_omp_threads, num_processes) \
               == mpi_cmd_str

    def test_get_mpi_args_srun(self):
        """Test MPI argument string for srun startup"""

        self.setup()
        test_config = self.config_file.copy()

        num_nodes = 2
        num_omp_threads = 16
        num_processes = 2

        test_config['mpi']['mpi_startup'] = "srun"
        test_config['mpi'].pop('mpi_root')

        mpi_cmd_str = "export OMPI_MCA_mpi_yield_when_idle=1 I_MPI_JOB_RESPECT_PROCESS_PLACEMENT=0 " \
                      "I_MPI_PIN_DOMAIN=omp:compact OMP_PROC_BIND=true OMP_PLACES=sockets " \
                      "OMP_NUM_THREADS=16 && srun " \
                      "--distribution=cyclic:cyclic:fcyclic " \
                      "--cpu-bind=verbose,sockets --overcommit --label --nodes=2 --ntasks=2 " \
                      "--cpus-per-task=16"

        assert imagingiobench.get_mpi_args(test_config, num_nodes, num_omp_threads, num_processes) \
               == mpi_cmd_str

    def test_get_telescope_config_settings(self):
        """Test telescope specific argument string"""

        param = {
            'vis_set': "lowbd2",
            'time_chunk': 32,
            'freq_chunk': 32
        }

        cmd_str = " --time -460:460/1024/32 --freq 260e6:300e6/8192/32 --target-err=1e-5 " \
                  "--margin=0"

        assert imagingiobench.get_telescope_config_settings(param) == cmd_str


if __name__ == '__main__':
    unittest.main(verbosity=2)
