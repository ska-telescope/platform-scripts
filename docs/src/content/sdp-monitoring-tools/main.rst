SDP Performance Metric Monitoring Tool
==========================================

The following sections provide the context, prerequisites and usage of the cpu metric monitoring toolkit.

.. toctree::
  :maxdepth: 1


  context
  prerequisites
  usage

API Documentation
----------------------

.. toctree::
  :maxdepth: 1

The following sections provide the API documentation of different files of the benchmark suite.

.. toctree::
  :maxdepth: 1

  ../../api/sdp-monitoring-tools/setup
  ../../api/sdp-monitoring-tools/swhwmetadata
  ../../api/sdp-monitoring-tools/monitormetrics
  ../../api/sdp-monitoring-tools/cpumetrics
  ../../api/sdp-monitoring-tools/perfmetrics
  ../../api/sdp-monitoring-tools/postprocessing
  ../../api/sdp-monitoring-tools/utils
  ../../api/sdp-monitoring-tools/processorspecific
  ../../api/sdp-monitoring-tools/exceptions
