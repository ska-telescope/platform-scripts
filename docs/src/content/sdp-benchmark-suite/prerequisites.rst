Prerequisites
==============================

The following prerequisites must be installed to use benchmark suite:

- python >= 3.6
- singularity
- git
- OpenMPI (with threading support)

For the Imaging IO test, we should have following dependencies installed.

- cmake
- `git lfs <https://git-lfs.github.com/>`_
- HDF5 (doesn't need to have parallel support)
- FFTW3

The benchmark suite **will not** install any missing dependencies and therefore it is necessary to install or load all the dependencies before running benchmark suite.

The benchmark suite is tested on both Linux and MacOS. It has not been tested on Windows and there is not guarantee that it will work on Windows systems.

On the MacOS, ``homebrew`` can be used to install all the listed
dependencies.

Installation
-----------------

Currently, the suite is not packaged into a python library. So, the only way to use is to clone the git repository and running the main script.

To set up the repository and get configuration files:

::

   git clone https://gitlab.com/ska-telescope/platform-scripts
   cd ska-sdp-benchmark-suite

To install all the required python modules

::

   pip3 install --user -r requirements.txt

If you would like to run unit tests as well, we should install the python modules in ``requirements-tests.txt`` too.

Configuration Files
---------------------

The benchmark suite uses configuration files to define various options. The configuration file is divided into two different pieces to abstract away the system based configuration from benchmark related configuration. Samples of these two configuration files can be found in the repository.

The default `benchmark configuration file <https://gitlab.com/ska-telescope/platform-scripts/-/blob/master/ska-sdp-benchmark-suite/sdpbenchmarks/config/default_config.yml>`_ defines global settings and benchmark related settings. The global settings can be overridden by the CLI. More details on the global settings are presented in :ref:`content/sdp-benchmark-suite/usage:Usage` section of the documentation.

The `system dependent configuration file <https://gitlab.com/ska-telescope/platform-scripts/-/blob/master/ska-sdp-benchmark-suite/sdpbenchmarks/config/.ska_sdp_bms_system_config.yml>`_ defines all the relevant settings based on the system the benchmark suite is running on. It includes names of the modules to load, MPI specific settings like mca parameters, interconnect, *etc*. and batch scheduler settings. Often these settings are defined once for the system and they do not have to be changed after.

It is recommended to copy this file to home directory of the user and modify the config file according to the system settings. By default the benchmark suite reads this config file from project directory (``platform-scripts/ska-sdp-benchmark-suite/sdpbenchmarks/config/.ska_sdp_bms_system_config.yml``) and user home directory (``$HOME/.ska_sdp_bms_system_config.yml``). The config file in the home directory has the precedence over the project directory. Different options in the configuration file are explained in the comments in each of the default files present in the repository.
