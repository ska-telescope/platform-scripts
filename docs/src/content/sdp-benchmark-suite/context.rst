About
==========

The aim of this package is to create a SDP benchmark suite with the available prototype pipelines that can be tested on different production HPC machines and hardwares. The development of a benchmark suite of this kind was proposed in SKA Computing Hardware Risk Mitigation Plan (SKA-TEL-SKO-0001083). This package automates the deployment of benchmarks, i.e., from compiling the code to parsing the output to get relevant metrics. The package is developed using modular approach and hence, as more prototype codes become available in the future, they can be readily integrated into this benchmark suite.

Available Benchmarks
---------------------

Currently, the benchmark suite contains only the `imaging IO <https://gitlab.com/ska-telescope/sdp/ska-sdp-exec-iotest>`_ test code developed by Peter Wortmann. More benchmark pipelines will be added to the suite in the future.

Imaging IO test
-----------------

The aim of this section is to give a high level overview of what imaging IO code does. The input to the code is a sky image and output is the visibility data. The code currently implements only the "predict" part of the imaging algorithm. More details can be found `here <https://on-demand.gputechconf.com/gtc/2017/presentation/s7125-bram-veenboer-efficient-imaging-handout.pdf>`_.
There are several input parameters to the benchmark, which can be found in the `documentation <https://developer.skatelescope.org/projects/ska-sdp-exec-iotest/en/latest/?badge=latest>`_ of the code. More details about the code and algorithms can be found in this `memo <http://ska-sdp.org/sites/default/files/attachments/distributed_predict_io_prototype_part_1_-_signed.pdf>`_.
