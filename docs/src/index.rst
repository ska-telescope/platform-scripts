.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:

Documentation of Platform Scripts
===================================

.. toctree::
  :maxdepth: 1

  ../content/sdp-benchmark-suite/main

  ../content/sdp-monitoring-tools/main

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
