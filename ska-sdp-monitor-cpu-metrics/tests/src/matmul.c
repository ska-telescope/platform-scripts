/******************************************************************************
* DGEMM simple CPU implementation
*
******************************************************************************/


// include headers
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

// Declaration of helper functions (see below for details)
float getGflops (int, double);

void dgemm_cpu_simple (double* a, double* b, double* c, int n) {

  int row, col, k;    // loop variables
  double val;         // help variable for results

   /*
     PERFORM MULTIPLICATION
  */
  // loop over output rows
  for ( row=0; row<n; row++ ) {

    // loop over output columns
    for ( col=0; col<n; col++ ) {

      // initialize output result to zero
      val = 0;

      // loop over inner dimension
      for ( k=0; k<n; k++ ) {

        // sum
        val += a[row*n+k] * b[k*n+col];
      }
      c[row*n+col] = val;
    }
  }


}

int main (int argc, char* argv[]) {

  int n = 10000;    // matrix dimension

  double *a ;         // matrices stored as row-major arrays
  double *b ;         // matrices stored as row-major arrays
  double *c ;         // matrices stored as row-major arrays

  int row, col;    // loop variables

  if (argc > 1) {
       n = atoi(argv[1]);
  }

  double elasped_time;

  // show banner
  printf ("\n\nMatrix-Multiplication \n");
  printf (    "==========================================\n");
  printf (  "\nSimple DGEMM implemantation on HOST");
  printf (  "\nMatrix size %d x %d", n, n);

  // allocate arrays and initialize data
  a = (double *) malloc ( n*n*sizeof(double) );
  b = (double *) malloc ( n*n*sizeof(double) );
  c = (double *) malloc ( n*n*sizeof(double) );

  for ( row = 0; row<n; row++ ) {
    for ( col = 0; col<n; col++ ) {
      // data is in row-major format
      a[row*n+col] = (row == col) ? 1.0 : 0.0;
      b[row*n+col] = row * n + col;
    }
  }


  for (int i = 0; i < 2; i++){
    time_t start = time(NULL);
    dgemm_cpu_simple (a, b, c, n);
    time_t end = time(NULL);


    // Get elapsed time for kernel execution
    elasped_time = difftime(end, start);
    printf ("\nTime: %4.4f; GFLOPS: %4.4f\n", elasped_time, getGflops(n, (double) elasped_time));
  }

  // write reference element for debugging
  //printf("\n     reference element c(768,768) = %4.4f <->  %4.4f\n", c[768*n+768], b[768*n+768] );

  // free memory
  free(a);
  free(b);
  free(c);

  return 0;

}


// get compute performance
float getGflops (int n, double time) {

  float gf = (2.0e-9 * n * n * n / time);
  return gf;

}
