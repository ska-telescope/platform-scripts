# SKA SDP Workflow Monitoring Tools

This toolkit contains the scripts to monitor the CPU related performance metrics and log them into JSON files. In its current implementation, the jobs must be submitted using SLURM for the toolkit to work.

More information on how to install, use the benchmark suite can be found in the [documentation](https://developer.skao.int/projects/platform-scripts/en/latest/?badge=latest).

**Note** that this repository has been reafactored and migrated to a new [repository](https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon). All the development will be made to this new repository.
