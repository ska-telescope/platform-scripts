"""This module contains utility functions for gathering CPU metrics"""

import os
import logging
import time
import subprocess
import psutil

from monitormetrics.utils.exceptions import JobPIDNotFoundError
from monitormetrics.utils.utils import find_procs_by_name, execute_cmd
from monitormetrics._version import __version__

_log = logging.LoggerAdapter(logging.getLogger(__name__), {'version': __version__})


class GetJobPID():
    """Class to get the main job PID for different workload managers. Currently SLURM, PBS and OAR
       schedulers are supported
    """

    def __init__(self, config):

        self.config = config.copy()

        # Get username
        self.user_name = os.environ['USER']
        # Get PID of the current script
        self.current_pid = os.getpid()
        # Get scheduler name
        self.scheduler = self.config['scheduler']
        # Get job ID
        self.job_id = self.config['job_id']
        # Get current node's hostname
        self.node_name = self.config['node_name']
        # Get SLURM submit node
        self.master_node = self.config['master_node']
        # Get launcher type
        self.launcher = self.config['launcher']
        # Get script name
        self.script_name = self.config['script_name']

        # define timeout (sec)
        self.timeout = 5
        # define maximum try outs (sec)
        self.max_retries = 5

    @staticmethod
    def get_proc_info(pid):
        """Convenient wrapper around psutil.Process to catch exceptions"""

        try:
            return psutil.Process(pid)
        except (psutil.AccessDenied, psutil.NoSuchProcess):
            return None

    def if_script_name_in_chilren(self, child_proc_names):
        """Return bool if script name in the list of names of children"""
        return any(map(self.script_name.__contains__, child_proc_names))

    def find_job_pid(self):
        """This method returns the job pid by iterating over all pids of the user and finding the
        parent process"""

        # List of job pids of mpirun/orted/slurmstepd/mpiexec/hydra_pmi_proxy
        launcher_pids = []

        # Iterate over all processes
        for proc in psutil.process_iter():
            if proc.username() == self.user_name and\
                    proc.name() in ['mpirun', 'orted', 'slurmstepd',
                                    'mpiexec', 'hydra_pmi_proxy', 'psid']:
                launcher_pids.append(proc.pid)

        found_pids = []
        for pid in launcher_pids:
            child_proc_names = []
            proc = self.get_proc_info(pid)
            if proc:
                for p in proc.children():
                    child_proc_names.append(p.name())
                _log.debug("Children processes names are %s" % child_proc_names)
                # If children do not have script name that is main job
                if not self.if_script_name_in_chilren(child_proc_names):
                    found_pids.append(pid)

        if found_pids:
            return found_pids
        else:
            # If pid is not found, raise an exception
            _log.error("Could not find job PID by naive method.")
            raise JobPIDNotFoundError("Could not find job PID.")

    def get_pbs_oar_job_pid(self, scheduler):
        """This method returns the pid of the OAR and PBS jobs"""
        # Typical spawning of processes on PBS looks as below. Here we assume sdpmonitormetrics is the
        # monitor job and hello is the main job
        # On head node:
        # bash(1)───pbs_mom(85)───bash(1069)─┬─13.pbs_master.S(1095)─┬─mpirun(1096)─┬─sdpmonitormetri(1102)───{sdpmonitormetri}(1103)
        #                                    │                       │              ├─ssh(1101)
        #                                    │                       │              └─{mpirun}(1099)
        #                                    │                       └─mpirun(1097)─┬─hello(1104)───{hello}(1110)
        #                                    │                                      ├─hello(1105)───{hello}(1112)
        #                                    │                                      ├─hello(1106)───{hello}(1115)
        #                                    │                                      ├─hello(1107)───{hello}(1118)
        #                                    │                                      ├─hello(1108)───{hello}(1114)
        #                                    │                                      ├─hello(1109)───{hello}(1117)
        #                                    │                                      ├─hello(1111)───{hello}(1119)
        #                                    │                                      ├─hello(1113)───{hello}(1116)
        #                                    │                                      ├─ssh(1100)
        #                                    │                                      └─{mpirun}(1098)
        #                                    └─pbs_demux(1070)
        # On slave nodes:
        # bash(1)─┬─pbs_mom(85)───{pbs_mom}(86)
        #         ├─sleep(93)
        #         └─sshd(163)─┬─sshd(497)───sshd(502)───orted(523)─┬─hello(543)───{hello}(552)
        #                     │                                    ├─hello(544)───{hello}(550)
        #                     │                                    ├─hello(545)───{hello}(558)
        #                     │                                    ├─hello(546)───{hello}(553)
        #                     │                                    ├─hello(547)───{hello}(554)
        #                     │                                    ├─hello(548)───{hello}(557)
        #                     │                                    ├─hello(549)───{hello}(555)
        #                     │                                    └─hello(551)───{hello}(556)
        #                     └─sshd(498)───sshd(501)───orted(503)───sdpmonitormetri(522)───{sdpmonitormetri}(528)
        # On head node we can use tracejob <jobid> command to get the pid of bash script, which in this case will yield
        # 1069. But tracejob command on slave nodes do not contain any PID information
        # So the best bet here is to look for process names. We see that mpirun is the one that spawns the processes
        # on the head node. We look for processes nameed mpirun and then check the process name of children
        # If the child doesnt contain sdpmonitormetrics (script name) that is the our main step job and we return PID
        # of that mpirun
        # In the case of slave nodes we spawning process looks quite complicated. We can looked for orted processes
        # and repeat the same process of checking for process names of children and filtering the main step job.

        # Job spawning process of OAR is very similar to PBS. Here is the schematic
        # Typical workflow of job launching in OAR on master node
        # systemd(1)───perl(7208)───perl(7209)───cpu-metrics-tes(7215)───mpirun(7350)─┬─matmul(7376)
        #                                                                             ├─ssh(7373)
        #                                                                             ├─{mpirun}(7369)
        #                                                                             ├─{mpirun}(7370)
        #                                                                             ├─{mpirun}(7371)
        #                                                                             └─{mpirun}(7372)
        # OAR as OAR user starts a perl process -> OAR starts perl process in user space -> Perl
        # process launches batch script -> Step jobs in batch script are launched
        # On rest of the nodes
        # systemd(1)───sshd(5103)───sshd(7381)───sshd(7387)───orted(7388)───matmul(7454)
        # So we should look for mpirun process on master node and orted process on slave nodes

        # In the case of Intel MPI there is an additional step between
        # mpiexec and jobs by hydra_pmi_proxy. So we check for that name

        # TODO: OAR and PBS have very similar job spawning workflows.
        #  Consider merging them into single method. We are effectively
        #  repeating code here

        # On head node we should look for mpirun process
        proc_name = self.launcher
        if self.launcher == 'mpirun':  # OpenMPI
            if self.master_node == self.node_name:
                proc_name = 'mpirun'
            else:
                # On slaves we should look for orted
                proc_name = 'orted'
        elif self.launcher == 'mpiexec':  # Intel MPI
            proc_name = 'hydra_pmi_proxy'

        start_time = time.time()
        num_try = 0
        while ((time.time() - start_time) < self.timeout or
               num_try < self.max_retries):
            # Get all processes with a proc_name
            mpirun_procs = find_procs_by_name(proc_name)
            _log.debug("Try %d: Found processes of name %s is %s" % (
                num_try, proc_name, mpirun_procs))
            found_pids = []
            for proc in mpirun_procs:
                job_pid = proc.pid
                child_procs = proc.children()
                child_proc_names = []
                # Get names of children processes
                for child_proc in child_procs:
                    child_name = child_proc.name()
                    if child_name not in ['ssh']:
                        child_proc_names.append(child_name)
                _log.debug("Try %d: Children processes names are %s" %
                           (num_try, child_proc_names))
                # If children do not have script name that is main job
                if (child_proc_names and
                        not self.if_script_name_in_chilren(child_proc_names)):
                    found_pids.append(job_pid)
                if found_pids:
                    return found_pids
            time.sleep(1)
            num_try += 1

        # If pid is not found by either of these methods, fallback to naive method
        _log.warning("Could not find %s job step PID on %s node. "
                     "Falling back to naive method" % (scheduler,
                                                       self.node_name))
        return self.find_job_pid()

    def get_slurm_mpirun_launcher_job_pid(self, step_id):
        """This method gets the job pids launched with mpirun launcher"""

        # CASE -> LAUNCHED BY MPIRUN
        # if mpirun is used to launch the jobs, typical spawning process is as follows:
        # systemd(pid0)───slurmstepd(pid1)───slurm_script(pid2)─┬─mpirun(pid3)─┬─sdpmonitormetri(pidn1)─┬─sdpmonitormetri(pidn2)
        #                                                       │                                       └─sdpmonitormetri(pidn3)
        #                                                       │
        #                                                       └─mpirun(pid4)─┬─srun(pid5)─┬─srun
        #                                                                      │            ├─{srun}
        #                                                                      │            ├─{srun}
        #                                                                      │            └─{srun}
        #                                                                      ├─stream(pid6)─┬─{stream}(pidm1)
        #                                                                                     |    ....
        #                                                                                     └─{stream}(pidmk)
        #
        # Typical scontrol listpids <job_id> will yield
        # PID      JOBID    STEPID LOCALID GLOBALID
        # pid2    <id>     batch  0       0
        # pid3    <id>     batch  -       -
        # pid4    <id>     batch  -       -
        # So for the case of mpirun, it is more simpler as we need to capture only pid4 which is always spawned
        # after monitor job. So it will be the last one.
        # These details are for the head nodes in the reservation. For slave nodes, it will be same albeit
        # there will not be any batch jobs.

        start_time = time.time()
        while (time.time() - start_time < self.timeout) and step_id >= 0:
            if self.node_name == self.master_node:
                cmd_str = ("scontrol listpids {} | awk '$3 == \"batch\" "
                           "{{print $1}}'".format(self.job_id))
            else:
                cmd_str = ("scontrol listpids {}.{} | awk 'NR > 1 "
                           "{{print $1}}'".format(self.job_id, step_id))

            all_pids = execute_cmd(cmd_str).splitlines()
            _log.debug("Output of %s is %s" % (cmd_str, all_pids))
            for pid in all_pids:
                try:
                    pid = int(pid)
                    proc = self.get_proc_info(pid)
                    if proc:
                        child_proc_names = [p.name() for p in
                                            proc.children(recursive=True)]
                        _log.debug(
                            "Child processes names are %s and parent "
                            "process name is %s" %
                            (child_proc_names, proc.name())
                        )
                        # Sometimes the process name is truncated. we need to to check a string for substrings contained in a list.
                        if (proc.name() in ['mpirun', 'orted'] and
                                proc.name() not in ['slurm_script', 'ssh']
                                and not self.if_script_name_in_chilren(
                                    child_proc_names)):
                            return [pid]
                except ValueError:
                    pass
            time.sleep(1)

        # If pid is not found by either of these methods, fallback to naive method
        _log.warning("Could not find SLURM job step PID on %s node. "
                     "Falling back to naive method" % self.node_name)
        return self.find_job_pid()

    def get_slurm_srun_launcher_job_pid(self, step_id):
        """This method gets the job pids launched with srun launcher"""

        # Finding Job PID in SLURM is tricky. It depends how the job is launched, ie, mpirun or srun.
        # We **strongly** recommend to launch both monitoring job and step job using same launcher
        #
        # CASE -> LAUNCHED BY SRUN
        # If srun is used to launch both step jobs, the spawning process is as follows:
        # For monitoring job it looks like this
        # systemd(pid0)───slurmstepd(pidn1)───sdpmonitormetri(pidn2)─┬─sdpmonitormetri(pidn3) ...
        #                                                            └─sdpmonitormetri(pidn4) ...
        # For main step job
        #
        # scontrol listpids <job_id> will yield a result as follows:
        # systemd(pid0)───slurmstepd(pidm1)─┬─stream(pidm2)─┬─{stream}
        #                                   │               |    ...
        #                                   │               └─{stream}
        #                                   |      .
        #                                   |      .
        #                                   |      .
        #                                   ├─stream(pidmk)─┬─{stream}
        #                                                   |    ...
        #                                                   └─{stream}
        # Typical scontrol listpids <job_id> will yield
        # PID      JOBID    STEPID LOCALID GLOBALID
        # pidm2    <id>     1      0       0
        #            ....
        # pidmk    <id>     1      k-1     k-1
        # pidb1    <id>     batch  0       0
        #            ....
        # pidbl    <id>     batch  -       -
        # pidn2    <id>     0      0       0
        #            ....
        # pidnn    <id>     0      -       -
        # Here pid0 is system PID which is 1, there are k main step jobs which are SLURM tasks. If we
        # sleep for couple of seconds after launching monitor job, we will ALWAYS have monitoring job
        # as step job 0. So, we check only for step job 1. All the step jobs of main job is spawned
        # by pidm1 and it is parent to all pids ranging from pidm2 to pidmk. So we check parents of
        # all step jobs of 1 and pass it as job PID.
        #

        start_time = time.time()
        while time.time() - start_time < self.timeout and step_id >= 0:
            cmd_str = ("scontrol listpids {}.{} | awk 'NR > 1 {{print "
                       "$1}}'".format(self.job_id, step_id))
            all_pids = execute_cmd(cmd_str).splitlines()
            _log.debug("Output of %s is %s" % (cmd_str, all_pids))
            captured_pids = []
            captured_ppids = []
            child_proc_names = []
            for pid in all_pids:
                try:
                    pid = int(pid)
                    proc = self.get_proc_info(pid)
                    if proc:
                        captured_pids.append(pid)
                        captured_ppids.append(proc.ppid())
                        child_proc_names.append(proc.name())
                except ValueError:
                    pass
            # remove duplicates
            captured_ppids = list(set(captured_ppids))
            _log.debug("Found job pids for step job %d are %s "
                       "and parent is %s" % (step_id,
                                             ",".join([str(p)
                                                       for p in
                                                       captured_pids]),
                                             ",".join([str(p)
                                                       for p in
                                                       captured_ppids])))
            if len(captured_ppids) == 1:
                if (psutil.Process(captured_ppids[0]).name() == 'slurmstepd'
                        and not self.if_script_name_in_chilren(child_proc_names)):
                    return captured_pids
            time.sleep(1)

        # If pid is not found by either of these methods, fallback to naive method
        _log.warning("Could not find SLURM job step PID on %s node. "
                     "Falling back to naive method" % self.node_name)
        return self.find_job_pid()

    def get_slurm_step_job_pid(self):
        """This method returns the pid of the SLURM step jobs"""

        for step_id in range(1, -1, -1):
            if self.launcher in ['srun', 'mpiexec']:
                return self.get_slurm_srun_launcher_job_pid(step_id)
            elif self.launcher == 'mpirun':
                return self.get_slurm_mpirun_launcher_job_pid(step_id)

    def find_pid(self):
        """This is the entry point for the class"""

        if self.scheduler == "SLURM":
            # If SLURM workload manager is found, get pid from scontrol listpids command
            return self.get_slurm_step_job_pid()
        elif self.scheduler in ["OAR", "PBS"]:
            # If OAR/PBS workload manager is found, get pid from mpirun and
            # orted procesees
            return self.get_pbs_oar_job_pid(self.scheduler)
        else:
            # If no supported workload manager is not found, check the user mpirun/orted/slurmstepd processes
            return self.find_job_pid()
