"""This file initiates the script to extract real time perf metrics"""

import os
import logging
import time

from monitormetrics.cpumetrics.cpumetrics import MonitorCPUMetrics
from monitormetrics.cpumetrics.perfmetrics import MonitorPerfEventsMetrics
from monitormetrics.utils.findpids import GetJobPID

from monitormetrics._version import __version__

_log = logging.LoggerAdapter(logging.getLogger(__name__), {'version': __version__})


class MonitorPerformanceMetrics():
    """Engine to extract performance metrics"""

    # pylint: disable=too-many-instance-attributes

    def __init__(self, config):
        """Initialize setup"""

        self.config = config
        self._extra = {}

    def get_job_pid(self):
        """This method calls function to get job PID"""

        # Get pid of the main job to monitor
        pid_getter = GetJobPID(config=self.config)
        self.config['pid'] = pid_getter.find_pid()

        _log.info("The monitoring process has the PID: %d", os.getpid())
        _log.info("The job to monitor has PID(s): %s", ",".join([str(p) for p in self.config['pid']]))

    def start_collection(self):
        """Start collecting CPU metrics. We use multiprocessing library to spawn different
        processes to monitor cpu and perf metrics"""

        self._extra['start_time'] = time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime())

        # Find job pid
        self.get_job_pid()

        # Spawn processes to monitor the job simultaneously.
        metric_processes = {}
        if 'cpu_metrics' in self.config['metrics']:
            metric_processes['cpu_metrics'] = MonitorCPUMetrics(config=self.config)
            metric_processes['cpu_metrics'].start()

        if 'perf_metrics' in self.config['metrics']:
            metric_processes['perf_metrics'] = MonitorPerfEventsMetrics(config=self.config)
            metric_processes['perf_metrics'].start()

        # Wait for the spawned processes to finish
        for _, spawned_proc in metric_processes.items():
            spawned_proc.join()

        self._extra['end_time'] = time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime())
