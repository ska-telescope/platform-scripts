"""This file does post-processing steps of the metric data collection"""

import os
import logging
import warnings
import datetime
import time
import yaml
import shutil
import matplotlib.pyplot
import numpy
import sqlite3
import pandas
import openpyxl

from monitormetrics.utils.utils import load_json, write_json, get_value, \
    replace_negative, PDF, FileLock
from monitormetrics._version import __version__

# Set matplotlib logging to warning. Else when logging is set to debugging, matplotlib is also
# logging the data.
logging.getLogger('matplotlib').setLevel(logging.WARNING)

_log = logging.LoggerAdapter(logging.getLogger(__name__), {'version': __version__})

# Generate colors for plots
COLORS = matplotlib.pyplot.rcParams['axes.prop_cycle'].by_key()['color']

# Bytes to MiB conversion factor
BYTES2MiB = 1048576

# Micro Joules to Joules conversion
UJ2J = 1000000

# Centimeters to inches conversion factor
CM2INCH = 1 / 2.54

# DPI resolution for the plots
DPI = 200

# matplotlib line styles
LINESTYLES = ['-', '--', '-.', ':']

# matplotlib global parameters
PARAMS = {'lines.markersize': 3,
          'legend.fontsize': 5,
          'axes.labelsize': 7,
          'axes.titlesize': 7,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6}
matplotlib.pyplot.rcParams.update(PARAMS)

# Ignore userwarnings from matplotlib, pandas
warnings.filterwarnings("ignore", category=UserWarning)


class MergeFiles():
    """This class contains methods to merge individual files from each node into a single file"""

    # pylint: disable=too-many-instance-attributes

    def __init__(self, config):
        """Initialize setup"""

        self.config = config.copy()

    def merge_data_files(self, root, filenames, metric_name):
        """Merge metric meta data files"""

        # Name of the merge JSON file
        merged_filename = os.path.join(self.config['data_dir'], metric_name + ".json")

        # Initialise the JSON file
        merged_file = {
            'host_names': []
        }

        # Add metric file from each host to the merged file
        for jsonfile in filenames:
            if jsonfile.startswith(str(self.config['job_id'])):
                content = load_json(os.path.join(root, jsonfile))
                try:
                    host_name = content['host_name']
                    merged_file['host_names'].append(host_name)
                    content.pop('host_name', None)
                    if metric_name in ["cpu_metrics", "perf_metrics"]:
                        merged_file['sampling_frequency'] = content['sampling_frequency']
                        content.pop('sampling_frequency', None)
                    merged_file[host_name] = content
                except KeyError:
                    pass

        # Write the merged file to disk
        write_json(merged_file, merged_filename)

    def merge_multi_json_files(self):
        """Merge json files from all nodes into single one"""

        _log.info("Merging all the output files into one single file")

        # Merge metric data from different hosts to a single file and delete raw data
        for metric in self.config['metrics']:
            raw_data_folder = self.config['temp_path'][metric]
            root, _, filenames = next(os.walk(raw_data_folder))
            self.merge_data_files(root, filenames, metric)

        _log.info("Successfully merged data from all nodes into single file")

        # Delete the raw data files
        raw_data_dir = os.path.dirname(self.config['temp_path'][metric])
        shutil.rmtree(raw_data_dir)

    def remove_ipc_file(self):
        """"Remove Inter Process Communicator file, if present"""

        if os.path.isfile(self.config['ipc_file']):
            os.remove(self.config['ipc_file'])

    def start(self):
        """Entry point for merging files"""

        _log.info("All nodes finished collecting metrics. Post processing the data files...")
        self.merge_multi_json_files()
        self.remove_ipc_file()


class CreateDataFrame():
    """This class contains all methods to create a dataframe from JSON data"""

    def __init__(self, metric, config):
        """Initialise setup"""

        self.config = config.copy()
        self.metric = metric

    def initialise_header_names(self):
        """This method initialises the names of headers for each metric"""

        # Header names of each monitored metric
        if self.metric == 'cpu_metrics':
            self.metric_header_names = {
                'time_stamps': "Timestamps",
                'cpu_percent': "CPU Percent [%]",
                'cpu_percent_sys': "System wide CPU Percent [%]",
                'cpu_time': "CPU Time [s]",
                'memory_full_info': {
                    'swap': "Swap memory [bytes]",
                    'uss': "USS memory [bytes]",
                },
                'memory_info': {
                    'rss': "RSS memory [bytes]",
                    'shared': "Shared memory [bytes]",
                    'vms': "VMS memory [bytes]",
                },
                'memory_percent': "System memory usage [%]",
                'io_counters': {
                    'read_bytes': "IO read [bytes]",
                    'write_bytes': "IO write [bytes]",
                    'read_count': "IO read counts",
                    'write_count': "IO write counts",
                },
                'net_io_counters': {
                    'bytes_recv': "Network recv data [bytes]",
                    'bytes_sent': "Network sent data [bytes]",
                    'packets_recv': "Network recv packets",
                    'packets_sent': "Network sent packets",
                },
                'num_fds': "# file descriptors",
                'num_threads': "# threads",
            }

        # Add perf event headers. We cannot have a homogenous headers as perf
        # events vary with different architectures and vendors. We use same
        # name for the header as the event name
        if self.metric == 'perf_metrics':
            try:
                perf_events = load_json(self.config['perf_event_file'])
                self.metric_header_names = {
                    'time_stamps': "Timestamps",
                    'hardware_events': {
                        'branch-misses:u': "Branch misses",
                        'branches:u': "# Branches",
                        'cache-misses:u': "Cache misses",
                        'cache-references:u': "Cache references",
                        'cycles:u': "Cycles",
                        'instructions:u': "Instructions",
                    },
                    'software_events': {
                        'context-switches:u': "Context switches",
                        'cpu-migrations:u': "CPU migrations",
                    },
                    'L2': {
                        key: key for key in perf_events['L2'].keys()
                    },
                    'L2cache': {
                        key: key for key in perf_events['L2cache'].keys()
                    },
                    'L3': {
                        key: key for key in perf_events['L3'].keys()
                    },
                    'L3cache': {
                        key: key for key in perf_events['L3cache'].keys()
                    },
                    'FLOPS_SP': {
                        key: key for key in perf_events['FLOPS_SP'].keys()
                    },
                    'FLOPS_DP': {
                        key: key for key in perf_events['FLOPS_DP'].keys()
                    },
                }
            except FileNotFoundError:
                pass

    def check_non_default_metrics(self, content):
        """Check for non default metrics"""

        # Memory bandwidth metrics
        if get_value(content, 'memory_bw'):
            self.metric_header_names = {
                **self.metric_header_names,
                'memory_bw': "Memory (read) bandwidth [MiB/s]",
            }

        # IB metrics
        if get_value(content, 'port_xmit_data'):
            self.metric_header_names = {
                **self.metric_header_names,
                'ib_io_counters': {
                    'port_rcv_data': "IB recv data [bytes]",
                    'port_xmit_data': "IB xmit data [bytes]",
                    'port_rcv_packets': "IB recv packets",
                    'port_xmit_packets': "IB xmit packets",
                },
            }

        # RAPL metrics - package, DRAM, core, uncore
        # we check if metrics available for upto 8 sockets
        rapl_metric_headers = {}
        for domain in ['package', 'dram', 'core', 'uncore']:
            for socket in range(8):
                metric_name = "-".join([domain, str(socket)])
                if get_value(content, metric_name):
                    rapl_metric_headers[metric_name] = "RAPL {} {} [uJ]".format(domain, socket)

        if rapl_metric_headers:
            self.metric_header_names = {
                **self.metric_header_names,
                'rapl_powercap': rapl_metric_headers,
            }

    def create_dataframe(self, content):
        """This method creates and returns dataframe from the metric data"""

        def join_header_to_host(header_name, host_name):
            return header_name + " ({})".format(host_name)

        # Get sampling interval
        sampling_interval = content['sampling_frequency']

        # Add non default metrics to metric header names if available
        self.check_non_default_metrics(content)

        # List of header names
        header_names = []

        # Number of entries for each metrics
        # Timestamps between different nodes can be different. We take maximum of all entries
        # not to miss any data
        num_entries = max([len(content[host]['time_stamps']) for host in
                           content['host_names']])

        # Initialise data frame data
        df_data = [[] for _ in range(num_entries)]

        # Make list of lists to create a dataframe. If each host has unequal number of
        # timestamps, we simply add zeros to additional entries.
        for metric, header in self.metric_header_names.items():
            metric_is_present = get_value(content, metric)
            if metric_is_present:
                if isinstance(header, str):
                    for host in content['host_names']:
                        header_names.append(
                            join_header_to_host(header, host))
                        add_ts = 0
                        for i in range(num_entries):
                            if i < len(content[host][metric]):
                                df_data[i].append(content[host][metric][i])
                            else:
                                if metric != 'time_stamps':
                                    df_data[i].append(0)
                                else:
                                    add_ts += 1
                                    df_data[i].append(
                                        content[host][metric][-1] +
                                        add_ts * sampling_interval
                                    )
                elif isinstance(header, dict):
                    for sub_metric, sub_header in header.items():
                        for host in content['host_names']:
                            header_names.append(
                                join_header_to_host(sub_header, host))
                            for i in range(num_entries):
                                if i < len(content[host][metric][sub_metric]):
                                    df_data[i].append(
                                        content[host][metric][sub_metric][i]
                                    )
                                else:
                                    df_data[i].append(0)

        # Create data frame
        return pandas.DataFrame(df_data, columns=header_names)

    def start(self):
        """Entry point to the class"""

        _log.info("Creating dataframe from %s data..." % self.metric)

        # Setup names of the metric headers
        self.initialise_header_names()

        # Load metrics data
        content = load_json(
            os.path.join(self.config['data_dir'], self.metric + ".json")
        )

        # If all monitor jobs on all nodes failed to monitor metrics we skip exporting
        if content['host_names']:
            return self.create_dataframe(content)
        else:
            _log.warning(
                "No %s data found. Skipping creating dataframe, "
                "and exporting to excel and databases" % self.metric
            )
            return None


class ExportExcelFile():
    """This class contains all methods to export dataframe into Excel files"""

    def __init__(self, metric, config, df):
        """Initialise setup"""

        self.metric = metric
        self.config = config.copy()
        self.df = df

    def get_lock_file(self):
        """Get the lock file to update excel sheets"""

        _log.info("Waiting for the lock file to lock excel sheets")

        # Name of the lock file. Prepending with "." to make it hidden
        self.config['excel_lock_file'] = ".excel_files"

        self.fl = FileLock(self.config['excel_lock_file'], timeout=10)
        while self.fl.lock_exists():
            time.sleep(1)

        _log.info("Acquired lock file to lock excel sheets")
        self.fl.acquire()

    def release_lock(self):
        """Releases lock file"""

        _log.info("Releasing excel sheets lock file")
        self.fl.release()

    def excel_exporter_engine(self):
        """This method exports the dataframe data into excel file"""

        # Initialise writer
        if os.path.isfile(self.config['xlsx_file'][self.metric]):
            book = openpyxl.load_workbook(
                self.config['xlsx_file'][self.metric]
            )
            writer = pandas.ExcelWriter(self.config['xlsx_file'][self.metric],
                                        engine='openpyxl')
            writer.book = book
        else:
            writer = pandas.ExcelWriter(self.config['xlsx_file'][self.metric],
                                        engine='openpyxl')

        # # create tuples from MultiIndex
        # df_to_tuples = self.df.columns.str.split(',', expand=True).values
        # self.df.columns = pandas.MultiIndex.from_tuples(
        #     [('', x[0]) if pandas.isnull(x[1]) else x for x in df_to_tuples])

        self.df.to_excel(writer,
                         sheet_name="Job ID {}".format(self.config['job_id']),
                         index=False
                         )
        writer.save()
        writer.close()

    def start(self):
        """Entry point to the class"""

        _log.info("Exporting %s data to excel file..." % self.metric)

        # Acquire lock to update sheet. Multiple jobs might try to update sheet at the same time.
        # We need to handle this race condition by file locks
        self.get_lock_file()

        # Create a pandas dataframe and save the dataframe into excel
        self.excel_exporter_engine()

        # Once we finish updating the sheet, release lock file
        self.release_lock()


class ExportDataBase():
    """This class contains all methods to export dataframe into SQL database"""

    def __init__(self, metric, config, df):
        """Initialise setup"""

        self.metric = metric
        self.config = config.copy()
        self.df = df

    def get_lock_file(self):
        """Get the lock file to update excel sheets"""

        _log.info("Waiting for the lock file to lock DB")

        # Name of the lock file. Prepending with "." to make it hidden
        self.config['db_lock_file'] = ".db_file"

        self.fl = FileLock(self.config['db_lock_file'], timeout=10)
        while self.fl.lock_exists():
            time.sleep(1)

        _log.info("Acquired lock file to lock DB")
        self.fl.acquire()

    def release_lock(self):
        """Releases lock file"""

        _log.info("Releasing DB lock file")
        self.fl.release()

    def db_exporter_engine(self):
        """This method exports the dataframe data into database"""

        # Initialise connection to DB
        conn = sqlite3.connect(self.config['db_file'])
        table_name = "_".join([self.metric, str(self.config['job_id'])])
        self.df.to_sql(table_name, conn, if_exists='append', index=False)

    def start(self):
        """Entry point to the class"""

        _log.info("Exporting %s data to db..." % self.metric)

        # Acquire lock to update sheet. Multiple jobs might try to update sheet at the same time.
        # We need to handle this race condition by file locks
        self.get_lock_file()

        # Create a pandas dataframe and save the dataframe into excel
        self.db_exporter_engine()

        # Once we finish updating the sheet, release lock file
        self.release_lock()


class MakePlots():
    """This class contains all plotting methods"""

    # pylint: disable=too-many-instance-attributes

    def __init__(self, config):
        """Initialize setup"""

        self.config = config.copy()

        # Initialise plot related parameters
        self.initialise_plot_parameters()

    def initialise_plot_parameters(self):
        """Initialises plot related parameters"""

        # Initialise x and y axis for plots
        self.x = []
        self.y = []
        self.yerr = []

        # Initialise time stamp ticks and ticklabels
        self.time_stamp_ticks = []
        self.time_stamp_tick_label = []

        # Initialise mean, max and stds of metric
        self.sum_metric = []
        self.mean_metric = []
        self.max_metric = []
        self.std_metric = []

        # Metrics to plot and their parameters
        self.metric_labels = {
            'cpu_percent_sys': {
                'name': "CPU Load",
                'cat': "CPU",
                'units': "%",
                'comb': "Average",
                'convert_to_rate': False,
                'unit_conversion': 1,
                'log_scale': False,
                'size': 8
            },
            'uss': {
                'name': "Memory",
                'cat': "Memory",
                'units': "MiB",
                'comb': "Average",
                'convert_to_rate': False,
                'unit_conversion': BYTES2MiB,
                'log_scale': False,
                'size': 8
            },
            'bytes_recv': {
                'name': "Input Traffic",
                'cat': "Network IO",
                'units': "MiB/s",
                'comb': "Total",
                'convert_to_rate': True,
                'unit_conversion': BYTES2MiB,
                'log_scale': False,
                'size': 7
            },
            'bytes_sent': {
                'name': "Output Traffic",
                'cat': "Network IO",
                'units': "MiB/s",
                'comb': "Total",
                'convert_to_rate': True,
                'unit_conversion': BYTES2MiB,
                'log_scale': False,
                'size': 7
            },
            'packets_recv': {
                'name': "Packets Input",
                'cat': "Network IO",
                'units': "#pkts/s",
                'comb': "Total",
                'convert_to_rate': True,
                'unit_conversion': 1,
                'log_scale': True,
                'size': 7
            },
            'packets_sent': {
                'name': "Packets Output",
                'cat': "Network IO",
                'units': "#pkts/s",
                'comb': "Total",
                'convert_to_rate': True,
                'unit_conversion': 1,
                'log_scale': True,
                'size': 7
            },
        }

    def get_timestamp_tick_labels(self, content):
        """This method resamples the timestamps for plotting and estimate tick locations and
        labels"""

        # Get list of timestamps
        hosts_time_stamps = get_value(content, 'time_stamps')
        len_time_stamps = [len(ts) for ts in hosts_time_stamps]
        time_stamps = hosts_time_stamps[len_time_stamps.index(max(len_time_stamps))]

        # Time stamps for average plots
        self.time_stamps = time_stamps

        # Set x ticks and corresponding tick labels
        self.time_stamp_ticks = numpy.array(time_stamps)[numpy.linspace(0, len(time_stamps) - 1, 10,
                                                                        dtype='int')].tolist()
        self.time_stamp_tick_label = [
            datetime.datetime.fromtimestamp(ts).strftime("%m/%d/%Y\n %H:%M:%S")
            for ts in self.time_stamp_ticks]

    def get_y_axis_values(self, content, metric, metric_att):
        """This method gets yaxis values for a given metric type"""

        y_axis_values = []
        if metric_att['cat'] in ["Energy"]:
            # We have more than 1 socket normally. Get values from each socket. We are looking
            # for 8 sockets here.
            for pkg_num in range(8):
                sub_metric = "-".join([metric, str(pkg_num)])
                values = get_value(content, sub_metric)
                if values:
                    # If metric is energy consumption, convert micro Joules to Joules for each
                    # package
                    y_axis_values.append([v / metric_att['unit_conversion'] for v in values[0]])
        else:
            # Convert units of metrics
            y_axis_values.append([v / metric_att['unit_conversion'] for v in
                                  get_value(content, metric)[0]])

        # Raw metrics are absolute value. To get rates, we are using forward differencing. Here we
        # are estimating IO bandwidth and packet transfer rate
        ya_vals = []
        for y_axis_value in y_axis_values:
            if metric_att['convert_to_rate'] and len(y_axis_value) > 1:
                y_axis_value = numpy.diff(y_axis_value) / numpy.diff(self.x)
                y_axis_value = y_axis_value.tolist()
                # For RAPL values, if energy counter exceeds max_energy_range_uj defined in the
                # powercap interface, it will reset the counter to zero. We will have a negative
                # power consumption in that case. We should correct them. Here if we have a
                # negative value, we replace that value with central averaging method
                y_axis_value = replace_negative(y_axis_value)
                y_axis_value += [y_axis_value[-1]]

            ya_vals.append(y_axis_value)

        return ya_vals

    def get_y_axis_values_glo_plots(self, metric, metric_att):
        """This method gets yaxis values for a average/total global plot of a given metric type"""

        if metric_att['units'] in ["W"]:
            metric += "_power"

        if metric_att['comb'] in ["Total"]:
            # For network, interconnect and energy data, we report total data for global plots
            self.y = self.sum_metric
            self.yerr = numpy.zeros(numpy.size(self.std_metric))
            self.label_prefix = "Total"
            self.fig_path = os.path.join(self.config['plot_dir'], metric + "_total.png")
        elif metric_att['comb'] in ["Average"]:
            # For the rest we report average metric over all nodes
            self.y = self.mean_metric
            self.yerr = self.std_metric
            self.label_prefix = "Average"
            self.fig_path = os.path.join(self.config['plot_dir'], metric + "_average.png")

    def get_mean_std_for_metric(self, all_host_values):
        """This method estimates mean, max and std values for a given metric using all hosts data"""

        # Pad the list of lists with zeros if they are of unequal sizes
        pad = len(max(all_host_values, key=len))
        all_host_values = numpy.array([i + [0] * (pad - len(i)) for i in all_host_values])

        # Estimate mean and max for all hosts combined
        self.sum_metric = numpy.sum(all_host_values, axis=0)
        self.mean_metric = numpy.mean(all_host_values, axis=0)
        self.max_metric = numpy.amax(all_host_values, axis=0)
        self.std_metric = numpy.std(all_host_values, axis=0)

    def apply_plot_settings(self, ax, metric_att):
        """This method applies the common settings to the plots"""

        # Set x axis name
        ax.set_xlabel("Time")

        # Set x tick locations and labels
        ax.set_xticks(self.time_stamp_ticks)
        ax.set_xticklabels(self.time_stamp_tick_label)

        # Plot log scale on y axis for bytes and packets
        if metric_att['log_scale']:
            ax.set_yscale('log', base=10)

        # Add total, mean, max information to the title of the plot
        ax.set_title("Average {0}/Node: {1:.3f} {3}, Max {0}/Node: {2:.3f} {3}".format(
            metric_att['name'], numpy.mean(self.mean_metric), numpy.amax(self.max_metric),
            metric_att['units']), weight='bold')

        # Place legend at upper right corner
        matplotlib.pyplot.legend(loc='upper right')
        # Add major and minor grid lines
        matplotlib.pyplot.grid(b=True, which='both', axis='both')
        # Choose tight layout
        matplotlib.pyplot.tight_layout()

    def plot_metric_each_host(self, content, metric, metric_att):
        """This method plots a given metric for all hosts"""

        _, ax = matplotlib.pyplot.subplots(figsize=(18 * CM2INCH, metric_att['size'] * CM2INCH))
        all_host_values = []
        # Get metric value for each host
        for ihost, host in enumerate(content['host_names']):
            self.x = content[host]['time_stamps']
            y_values = self.get_y_axis_values(content[host], metric, metric_att)

            for i_line, y_val in enumerate(y_values):
                # If returned empty, fill it with zeros.
                if not y_val:
                    y_val = [0 for _ in range(len(self.x))]
                ax.plot(self.x, y_val, color=COLORS[ihost], marker='o',
                        linestyle=LINESTYLES[i_line], markerfacecolor=COLORS[ihost], label=host)
                all_host_values.append(y_val)

        # Estimate mean, max, sum and STD values of metric with data from all hosts
        self.get_mean_std_for_metric(all_host_values)

        # Append power to RAPL metric names
        if metric_att['cat'] in ["Energy"]:
            metric += "_power"

        ax.set_ylabel(" ".join([metric_att['name'], "[{}]".format(metric_att['units'])]))
        self.apply_plot_settings(ax, metric_att)
        fig_path = os.path.join(self.config['plot_dir'], metric + "_per_node.png")
        matplotlib.pyplot.savefig(fig_path, dpi=DPI)
        matplotlib.pyplot.close()

    def plot_combined_metric(self, metric, metric_att):
        """This method plots the global (mean/total) data for a given metric"""

        _, ax = matplotlib.pyplot.subplots(figsize=(18 * CM2INCH, metric_att['size'] * CM2INCH))
        # Plot combined metric with std as error in the errorbar
        self.x = self.time_stamps
        self.get_y_axis_values_glo_plots(metric, metric_att)
        ax.errorbar(self.x, self.y, self.yerr, color=COLORS[0], marker='o',
                    markerfacecolor=COLORS[0], ecolor='lightgray',
                    label=" ".join([self.label_prefix, metric_att['name']]))

        ax.set_ylabel(" ".join([self.label_prefix, metric_att['name'],
                                "[{}]".format(metric_att['units'])]))
        self.apply_plot_settings(ax, metric_att)
        matplotlib.pyplot.savefig(self.fig_path, dpi=DPI)
        matplotlib.pyplot.close()

    def check_non_default_metrics(self, content):
        """Check if IB, mem. bandwidth and RAPL metrics are available in collected metrics"""

        # IB metrics
        if get_value(content, 'port_xmit_data'):
            self.metric_labels = {
                **self.metric_labels,
                'port_rcv_data': {
                    'name': "Interconnect Input Traffic",
                    'cat': "Network IO",
                    'units': "MiB/s",
                    'comb': "Total",
                    'convert_to_rate': True,
                    'unit_conversion': BYTES2MiB,
                    'log_scale': False,
                    'size': 7
                },
                'port_xmit_data': {
                    'name': "Interconnect Output Traffic",
                    'cat': "Network IO",
                    'units': "MiB/s",
                    'comb': "Total",
                    'convert_to_rate': True,
                    'unit_conversion': BYTES2MiB,
                    'log_scale': False,
                    'size': 7
                },
                'port_rcv_packets': {
                    'name': "Interconnect Packets Input",
                    'cat': "Network IO",
                    'units': "#pkts/s",
                    'comb': "Total",
                    'convert_to_rate': True,
                    'unit_conversion': 1,
                    'log_scale': True,
                    'size': 7
                },
                'port_xmit_packets': {
                    'name': "Interconnect Packets Output",
                    'cat': "Network IO",
                    'units': "#pkts/s",
                    'comb': "Total",
                    'convert_to_rate': True,
                    'unit_conversion': 1,
                    'log_scale': True,
                    'size': 7
                },
            }

        # Memory bandwidth metrics
        if get_value(content, 'memory_bw'):
            self.metric_labels = {
                **self.metric_labels,
                'memory_bw': {
                    'name': "Memory Bandwidth (Read)",
                    'cat': "Memory",
                    'units': "MiB/s",
                    'comb': "Average",
                    'convert_to_rate': False,
                    'unit_conversion': BYTES2MiB,
                    'log_scale': False,
                    'size': 8
                },
            }

        # RAPL metrics - package
        if get_value(content, 'package-0'):
            self.metric_labels = {
                **self.metric_labels,
                'package': {
                    'name': "RAPL Package Power",
                    'cat': "Energy",
                    'units': "W",
                    'comb': "Total",
                    'convert_to_rate': True,
                    'unit_conversion': UJ2J,
                    'log_scale': False,
                    'size': 7
                },
            }

        # RAPL metrics - DRAM
        if get_value(content, 'dram-0'):
            self.metric_labels = {
                **self.metric_labels,
                'dram': {
                    'name': "RAPL DRAM Power",
                    'cat': "Energy",
                    'units': "W",
                    'comb': "Total",
                    'convert_to_rate': True,
                    'unit_conversion': UJ2J,
                    'log_scale': False,
                    'size': 7
                },
            }

        # RAPL metrics - Core
        if get_value(content, 'core-0'):
            self.metric_labels = {
                **self.metric_labels,
                'core': {
                    'name': "RAPL Core Power",
                    'cat': "Energy",
                    'units': "W",
                    'comb': "Total",
                    'convert_to_rate': True,
                    'unit_conversion': UJ2J,
                    'log_scale': False,
                    'size': 7
                },
            }

        # RAPL metrics - Uncore
        if get_value(content, 'uncore-0'):
            self.metric_labels = {
                **self.metric_labels,
                'uncore': {
                    'name': "RAPL Uncore Power",
                    'cat': "Energy",
                    'units': "W",
                    'comb': "Total",
                    'convert_to_rate': True,
                    'unit_conversion': UJ2J,
                    'log_scale': False,
                    'size': 7
                }
            }

    def plot_metric_data(self, content):
        """Make plots for the cpu metric data"""

        # Check the availability of IB, memory bandwidth and RAPL metrics and add to metric label
        # dict
        self.check_non_default_metrics(content)

        # Get list of timestamp tick locations and labels
        self.get_timestamp_tick_labels(content)

        for metric, metric_att in self.metric_labels.items():
            # Plot metrics per host
            self.plot_metric_each_host(content, metric, metric_att)

            # Plot the mean/total and error bars from std for all hosts combined
            self.plot_combined_metric(metric, metric_att)

    def start(self):
        """Entry point for plotting"""

        _log.info("Making plots...")

        # Load CPU metrics data
        content = load_json(os.path.join(self.config['data_dir'], "cpu_metrics.json"))

        if content['host_names']:
            self.plot_metric_data(content)
            _log.info("Plots generated")
        else:
            _log.warning("No data found. Skipping plots generation")


class GenerateReport():
    """This class does all the post monitoring steps like making plots and generating reports"""

    # pylint: disable=too-many-instance-attributes

    def __init__(self, config):
        """Initialize setup"""

        self.config = config.copy()

        # Initialise plot related parameters
        self.initialise_plot_per_page()

    def initialise_plot_per_page(self):
        """Initialises plot related parameters"""

        # Define plots per page
        self.plots_per_page = [["cpu_percent_sys_average.png", "uss_average.png",
                                "memory_bw_average.png"],
                               ["bytes_sent_total.png", "bytes_recv_total.png",
                                "packets_sent_total.png", "packets_recv_total.png"],
                               ["port_xmit_data_total.png", "port_rcv_data_total.png",
                                "port_xmit_packets_total.png", "port_rcv_packets_total.png"],
                               ["package_power_total.png", "dram_power_total.png",
                                "core_power_total.png", "uncore_power_total.png"],
                               ["cpu_percent_sys_per_node.png", "uss_per_node.png",
                                "memory_bw_per_node.png"],
                               ["bytes_sent_per_node.png", "bytes_recv_per_node.png",
                                "packets_sent_per_node.png", "packets_recv_per_node.png"],
                               ["port_xmit_data_per_node.png", "port_rcv_data_per_node.png",
                                "port_xmit_packets_per_node.png", "port_rcv_packets_per_node.png"],
                               ["package_power_per_node.png", "dram_power_per_node.png",
                                "core_power_per_node.png", "uncore_power_per_node.png"]
                               ]

    def create_job_report(self, content):
        """Create a job report using FPDF module"""

        # Number of nodes in the job
        num_nodes = len(content['host_names'])
        # Get username of the job
        user_name = os.environ['USER']

        # Add them to the config file
        self.config['user'] = user_name
        self.config['num_nodes'] = num_nodes

        # Initiate the class
        pdf = PDF(self.config)

        # Add each page to pdf
        for elem in self.plots_per_page:
            image_list = []
            for image in elem:
                image_path = os.path.join(self.config['plot_dir'], image)
                if os.path.isfile(image_path):
                    image_list.append(image_path)
            pdf.print_page(image_list)

        # Finally save pdf
        pdf.output(self.config['report_path'], 'F')

    def start(self):
        """Entry point for creating report"""

        _log.info("Creating job report...")

        # Load CPU metric data
        content = load_json(os.path.join(self.config['data_dir'], "cpu_metrics.json"))

        if content['host_names']:
            self.create_job_report(content)
            _log.info("Job report generated")
        else:
            _log.warning("No metric data found. Skipping job report generation.")


class PostMonitoring():
    """This class performs post monitoring steps"""

    # pylint: disable=too-many-instance-attributes

    def __init__(self, config):
        """Initialize setup"""

        self.config = config.copy()

    def dump_config(self):
        """Dump config files (for debugging)"""

        # Create a directory to save config files
        os.makedirs(os.path.join(self.config['save_dir'], "configs"),
                    exist_ok=True)
        # Save running config
        with open(os.path.join(self.config['save_dir'], "configs",
                               "_".join([self.config['host_name'],
                                         'run_config.yml'])),
                  'w') as conf_file:
            yaml.dump(self.config, conf_file)

    def start(self):
        """Entry point for post monitoring steps"""

        # Dump config file information (for debugging)
        if self.config['verbose']:
            self.dump_config()

        # We want to run these steps only on one node. We use master node
        if self.config['master_node'] == self.config['node_name']:
            # Run MergeFiles class
            merge_files = MergeFiles(config=self.config)
            merge_files.start()

            # Export results to excel and SQL database
            for metric in [m for m in self.config['metrics'] if
                           m != 'meta_data']:
                metric_df = CreateDataFrame(metric=metric, config=self.config)
                df = metric_df.start()
                if df is not None and self.config['export_xlsx']:
                    export_to_excel = ExportExcelFile(metric=metric,
                                                      config=self.config,
                                                      df=df)
                    export_to_excel.start()
                if df is not None and self.config['export_db']:
                    export_to_db = ExportDataBase(metric=metric,
                                                  config=self.config,
                                                  df=df)
                    export_to_db.start()

            # Generate plots and report only if asked at CLI
            if self.config['gen_report']:
                # Run MakePlots class
                make_plots = MakePlots(config=self.config)
                make_plots.start()

                # Run GenerateReport class
                gen_report = GenerateReport(config=self.config)
                gen_report.start()
