#!/usr/bin/env python

import sys
from setuptools import setup
import setuptools

from monitormetrics._version import __version__

# Bail on Python < 3
assert sys.version_info[0] >= 3

# Load local readme file
with open("README.md") as readme_file:
    readme = readme_file.read()

# List of all packages
packages = [
    "monitormetrics",
    "monitormetrics/cpumetrics",
    "monitormetrics/gpumetrics",
    "monitormetrics/prepostmonitoring",
    "monitormetrics/utils",
    "monitormetrics/perfevents",
]

package_data = {
    'monitormetrics': [
        'perfevents/*.yml'
    ]
}

# List of all required packages
reqs = [
    line.strip()
    for line in open("requirements.txt").readlines()
]

# setup config
setup(
    name="ska-sdp-monitor-metrics",
    version=__version__,
    python_requires=">=3.7",
    description="A toolkit to monitor performance metrics for the jobs submitted using workload "
                "manager like SLURM",
    long_description=readme + "\n\n",
    maintainer="Mahendra Paipuri",
    maintainer_email="mahendra.paipuri@inria.fr",
    url="https://gitlab.com/ska-telescope/platform-scripts",
    project_urls={
        "Documentation": "https://developer.skao.int/projects/platform-scripts/en/latest/?badge=latest",
        "Source": "https://gitlab.com/ska-telescope/platform-scripts",
    },
    zip_safe=False,
    classifiers=[
        "Development Status :: Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    packages=packages,
    package_data=package_data,
    scripts=['scripts/sdpmonitormetrics'],
    install_requires=reqs,
)
